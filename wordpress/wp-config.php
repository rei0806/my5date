<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'wordpress' );

/** MySQL database password */
define( 'DB_PASSWORD', 'wordpress' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '!9l=bJ;amjX@B9tIuw!]8VZ]yI:gvyxa3!97XpGKnG&uElPMcjKe>bj2X Ao>C=-' );
define( 'SECURE_AUTH_KEY',   '@hp3q_YfqUfg)0j{+Q<P/-n$(VQPBNA4@(;R{sbU3YYi wwL>ao]#;Iyq&HGt|kF' );
define( 'LOGGED_IN_KEY',     'tZ!;Bn%)/NPXrLsJ=sZgU^ByOjN^b;W sT0`hzp$Vb/(DP?`6p#@uvE|cH#]zWAi' );
define( 'NONCE_KEY',         'F|Sjut([U|74? I!xmVCt0A$F|d_Mm:xl#~$6)Z*s(DFQKn[BS=}3h@U  *|2hU:' );
define( 'AUTH_SALT',         '_=/mEtRNOH?O5?jrJ8xRJZC-l}FlWjT%b35K!~jqxs@7t.eK9vh[F7:wuIqIx1oe' );
define( 'SECURE_AUTH_SALT',  'S=[..pPzL;!HeBs_/hIknYiE2o2.qa6[V8hI]<Lf3V.KlU/.`s_PLyLJh!pa5i.S' );
define( 'LOGGED_IN_SALT',    'asv#XkA&&tH#g;33561+i-%Zt9ocCJa3QoLcB!3-4+N,vA>oKmX_,NHM8UjPNR=e' );
define( 'NONCE_SALT',        ')S_cw%Y1tVDY.~4E3Q5l{EGKS/^ f4o,_TC1lkRcp%?[A>cP~vP|vBl2Y=-=OkY+' );
define( 'WP_CACHE_KEY_SALT', 'TCR?MhCV%OCcW8 %S^]K@j]Z{r_dMu7?DwkTOI/NmAt%3q:o[D%g8|<ICG,0tCJE' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define( 'JETPACK_DEV_DEBUG', True );
define( 'WP_DEBUG', True );
define( 'FORCE_SSL_ADMIN', False );
define( 'SAVEQUERIES', False );

// Additional PHP code in the wp-config.php
// These lines are inserted by VCCW.
// You can place additional PHP code here!


/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
