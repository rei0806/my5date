const { series, watch, src, dest } = require('gulp');
const webpackStream = require("webpack-stream");
const webpack = require("webpack");

const paths = {
  rootDir: "dist/",
  scssSrc: "src/sass/**/*.scss",
  jsSrc: "src/js/main.js",
  outCss: "dist/css",
  outJs: "dist/js",
  parentScssSrc: "../ztheme/src/sass/**/*.scss",
};

// webpack
const webpackConfig = require("./webpack.config");
function exec_webpack(cb) {
  webpackStream(webpackConfig, webpack)
    .pipe(dest(paths.outJs));
  cb();
}
exports.webpack = series(exec_webpack);

// CSSをコンパイル
const sass = require('gulp-sass');
const postcss = require("gulp-postcss");
const autoprefixer = require("autoprefixer");
function compile_css(cb) {
  src(paths.scssSrc)
    .pipe(sass({outputStyle: "compressed"})) // SassからCSSにする
    .pipe(postcss([ // ベンダープレフィックスをつける
      autoprefixer({ cascade: false })  // NOTE: 対象ブラウザは.browserslistで指定している
    ]))
    .pipe(dest(paths.outCss)); // 出力する
  cb();
}
exports.sass = series(compile_css);


// 監視してタスクを実行
function watch_and_exec(cb) {
  watch(paths.scssSrc, compile_css);
  watch(paths.jsSrc, exec_webpack);
  watch(paths.parentScssSrc, compile_css);
  cb();
}
exports.default = series(watch_and_exec);
