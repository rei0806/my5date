import '@babel/polyfill';

import Vue from 'vue';

document.addEventListener('DOMContentLoaded', function(){

  let App = new Vue({
    el: '#vue-app',
    components: {
    },
  });

  /*----------------------------------------------------
   * NOTE: jQueryを動かしたいなら、ここから下で実行すること
   *---------------------------------------------------*/

  // ハンバーガーメニュー
  jQuery('.js-hamburgerMenuBtn, .js-closeNavModalBtn').click(function () {
    jQuery('.js-overlayModalArea').fadeToggle(100);
  });

});
