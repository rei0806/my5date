module.exports = {
  // モード値を production に設定すると最適化された状態で、
  // development に設定するとソースマップ有効でJSファイルが出力される
  mode: "production",
  //mode: "development",

  // メインのJS
  entry: "./src/js/main.js",
  // 出力ファイル
  output: {
    filename: "./bundle.js"
  },
  resolve: {
    alias: {
      // vueの完全ビルドのための設定（templateオプション、DOM 内のHTMLをテンプレートとして利用し要素にマウントする場合は必要）
      // 参照: https://jp.vuejs.org/v2/guide/installation.html#ランタイム-コンパイラとランタイム限定の違い
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$/, // 拡張子 .js の場合
        use: [
          {
            loader: "babel-loader", // Babel を利用する
            options: {
              presets: [
                [ "@babel/preset-env", {
                  useBuiltIns: 'entry',
                  corejs: 3,
                }]
              ],
            }
          }
        ]
      }
    ]
  }
}
